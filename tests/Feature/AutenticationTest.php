<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;

class AutenticationTest extends TestCase
{

    public function test_login() : void
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/auth', [
            'password' => 'password',
            'username' => $user->username
        ]);
        $response->assertStatus(200);
    }

    public function testLoginSuccesfull() : void
    {
        $data = ['username' => 'olaya', 'password' => 'password'];
        $response = $this->call('POST', 'api/auth', $data);
        $this->assertEquals(200, $response->status());
        $content = json_decode($response->getContent());
        $this->assertObjectHasAttribute('token', $content->data);
        $this->assertNotEmpty($content->data->token);
    }

}
