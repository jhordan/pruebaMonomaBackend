<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Candidato;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;

class CreateCandidatoTest extends TestCase
{
    //use DatabaseTransactions;
    private $model;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = new Candidato();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_createCandidato()
    {
        $candidato   = [
            'name'  => 'pepito',
            'source' => 'Fotocasa',
            'owner' => 1,
            'created_by'  => 1
        ];
        $response = $this->model->create($candidato);
        $this->assertInstanceOf(Candidato::class, $response);
    }

}
