<?php

namespace Tests\Feature;


use Tests\TestCase;
use App\Models\User;
use App\Models\Candidato;
use App\Http\Controllers\CandidatoController;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class GetCandidatosTest extends TestCase
{

    use  WithoutMiddleware;

    private $model;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = new Candidato();
    }



     /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_getCandidatos() : void
    {

        $user = User::all()->random();
        $this->actingAs($user);
        $response = $this->json('GET', 'api/leads', [
            'role'      => $user->role
        ]);

        $response->assertStatus(200);
    }

     /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_updateCandidatosIdCreated() : void
    {
        $candidato = Candidato::factory()->create();
        $dataUpdate = [
            'name'  => 'Los pepes'
        ];
        $response = $this->model->findOrFail($candidato->id)->update($dataUpdate);
        $this->assertTrue($response);

    }

     /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_getCandidatosId() : void
    {
        $response = $this->get('api/lead/4');
        $candidato_id = Candidato::findOrFail(4);
        $response->assertStatus(200);

    }
}
