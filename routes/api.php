<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CandidatoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('auth',[AuthController::class,'loginUser']);
/***routes with token */
Route::middleware('jwt.token')->group(function(){
    Route::get('leads',[CandidatoController::class,'index']);
    Route::post('lead',[CandidatoController::class,'store']);
    Route::get('lead/{id}',[CandidatoController::class,'show']);
});
