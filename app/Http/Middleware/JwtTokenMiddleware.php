<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;




class JwtTokenMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        try {
            //code...
            JWTAuth::parseToken()->authenticate();
            $request->merge(['user' => auth('api')->user()]);
        } catch (Exception $th) {
            //throw $th;
            if($th instanceof TokenInvalidException) {

                return response()
                    ->json([
                        'meta' => ['success' => 'false','errors' => ['Invalid Token']]
                    ], Response::HTTP_UNAUTHORIZED)
                    ->withCallback($request->input('callback'));
            }

            if($th instanceof TokenExpiredException) {
                return response()->json(['status' => 'Token expired']);

                return response()
                    ->json([
                        'meta' => ['success' => 'false','errors' => ['Token expired']]
                    ], Response::HTTP_UNAUTHORIZED)
                    ->withCallback($request->input('callback'));
            }

            else {
                return response()
                    ->json([
                        'meta' => ['success' => 'false','errors' => ['Authorization Token not found']]
                    ], Response::HTTP_UNAUTHORIZED)
                    ->withCallback($request->input('callback'));
            }
        }

          return $next($request);
    }
}
