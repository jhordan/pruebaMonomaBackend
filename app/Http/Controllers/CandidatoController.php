<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Candidato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\ValidateSaveCandidatoRequest;


class CandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if($this->getRolUser() == 'manager') {
            $candidato = Candidato::all();
            return response()->json($candidato);
        } else {
            $candidato = Candidato::all()->where('owner',auth()->user()->id);
            return response()->json($candidato);
        }

    }

    /**
     * obtener rol el usuario logueado
     *
     * @return String
     */
    public function getRolUser() : string {
        return auth()->user()->role;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateSaveCandidatoRequest $request)
    {
            $candidato = Candidato::create([
            'name' => $request->name,
            'source'=> $request->source,
            'owner'=> $request->owner,
            'created_by'=> auth()->user()->id,
            ]);
            return response()
            ->json([
            'meta' => ['success' => 'true','errors' => []],
            'data' => [$candidato]
            ], Response::HTTP_CREATED);
        DB::beginTransaction();
        try {
            if($this->getRolUser() == 'manager'){
                $candidato = Candidato::create([
                    'name' => $request->name,
                    'source'=> $request->source,
                    'owner'=> $request->owner,
                    'created_by'=> auth()->user()->id,
                ]);
                DB::commit();
                return response()
                    ->json([
                        'meta' => ['success' => 'true','errors' => []],
                        'data' => [$candidato]
                    ], Response::HTTP_CREATED);
            } else {
                return response()
                ->json([
                    'meta' => ['success' => 'false','errors' => ['Solo  puede crear usuarios el usuario con rol "Manager"']]
                ], Response::HTTP_UNAUTHORIZED);
            }

        } catch (\Exception $e) {
            DB::rollback();

            Log::debug('El error fue por' . $e->getMessage());

            return response()
                ->json([
                    'meta' => ['success' => 'false','errors' => $e->getMessage()]
                ], Response::HTTP_INTERNAL_SERVER_ERROR)
                ->withCallback($request->input('callback'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $candidato
     * @return \Illuminate\Http\Response
     */
    public function show($candidato)
    {

        try {
            $candidato_id = Candidato::findOrFail($candidato);

            return response()
                ->json([
                    'meta' => ['success' => 'true','errors' => []],
                    'data' => [$candidato_id]
                ], Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();

            Log::debug('El error fue por' . $e->getMessage());

            return response()
                ->json([
                    'meta' => ['success' => 'false','errors' => 'No lead found']
                ], Response::HTTP_NOT_FOUND);
        }
    }
}
