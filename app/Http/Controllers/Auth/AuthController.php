<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\Auth\LoginUserRequest;
use Symfony\Component\HttpFoundation\Response;


class AuthController extends Controller
{
     /**
     * loginUser.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginUser(LoginUserRequest $request) {

        $credentials = $request->only('username', 'password','email');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {

                return response()
                    ->json([
                        'meta' => ['success' => 'false','errors' => ['Password incorrect for:' .$request->username]]
                    ], Response::HTTP_UNAUTHORIZED)
                    ->withCallback($request->input('callback'));

            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()
            ->json([
                'meta' => ['success' => 'true','errors' => []],
                'data' => ['token' =>$token,'minutes_to_expire' =>  $_ENV['JWT_TTL']]
            ], Response::HTTP_OK)
            ->withCallback($request->input('callback'));
    }
}
