<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Candidato;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CandidatoDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersId= DB::table('users')->pluck('id');
        $usersIdCreated= DB::table('users')->where('role' , 'manager')->pluck('id');
        $faker = Faker::create();
        foreach (range(1,20) as $index) {
            DB::table('candidato')->insert(
                [
                    'name'=>$faker->name(),
                    'source'=>$faker->randomElement(['Fotocasa', 'Principal','Secundaria']),
                    'owner'=>$faker->randomElement($usersId),
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'created_by'=>$faker->randomElement($usersIdCreated)
                ]
            );
        }

        Candidato::factory()->count(6)->create();
    }
}
