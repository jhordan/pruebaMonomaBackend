<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\CandidatoDefault;
use Database\Seeders\UserRolAgentDefault;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        /***llama seeders */
        $this->call([
            UserRolAgentDefault::class,
            UserRolManagerDefault::class,
            CandidatoDefault::class
        ]);
    }
}
