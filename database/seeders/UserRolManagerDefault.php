<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Carbon\Traits\Date;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRolManagerDefault extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'username'=>Str::random(2),
                'password'=>Hash::make('password'),
                'email'=> Str::random(2).'@gmail.com',
                'last_login' => Carbon::now()->format('Y-m-d H:i:s'),
                'is_active'=>true,
                'role'=>'manager'
            ]
        );

        User::factory()->create([
            'username'=>'olaya',
            'role' => 'manager',
            'email' => 'jhojamil92a@gmail.com'
        ]);

        User::factory()->count(2)->create([
            'username'=>'miller',
            'role' => 'manager'
        ]);
    }
}
