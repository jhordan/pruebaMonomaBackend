**Prueba de desarrollo.**
Prueba de desarrollo para **MONOMA**.

**Paquetes Terceros**
- jwt-auth (https://jwt-auth.readthedocs.io/en/develop/laravel-installation/)

**Pre-requisitos**
- Php 7.3.0 con phpCli habilitado para la ejecución de comando.
- MySQL
- Composer
- Extensión pdo_pgsql habilitada.

**Instalación**

1. Clonar el repositorio en la carperta del servidor web.
[git clone git@gitlab.com:jhordan/pruebaMonomaBackend.git]
[git clone https://gitlab.com/jhordan/pruebaMonomaBackend.git]

2. Instalar paquetes.
`composer install`

3. Configurar archivo .env (Se envia el archivo ya configurado con las variables de entorno incluyendo la del JWT_TTL para el tiempo de expiración del token)

4. Configure las variables de entorno para base de datos

- `DB_HOST=` Variable de entorno para el host de BD.
- `DB_PORT=` Variable de entorno para el puerto de BD.
- `DB_DATABASE=` Variable de entorno para el nombre de BD.
- `DB_USERNAME=` Variable de entorno para el usuario de BD.
- `DB_PASSWORD=` Variable de entorno para la contraseña de BD.


5. En la raíz del sitio ejecutar.

- php artisan key:generate Genera la llave para el cifrado del proyecto.
- composer install Instala dependencias de PHP
- php artisan migrate:refresh --seed Ejecuta migraciones y seeders

6. Usuarios Predefinidos.

| Username | Pass | Role|
| ------ | ------ |------ |
| olaya | password | Manager
| jhordan | password | agent
| cell | cell | cell |


**Proceso:**

1. Se crearon los endpoints deacuerdo como se planteo en el .pdf.
2. `http://127.0.0.1:8000/api/auth`, tipo POST, para la utenticación del usuario a loguear de alli obtenemos el token por Jwt
3. `http://127.0.0.1:8000/api/leads`, tipo GET, para obtener todos los candidatos creados, se envia token en la petición
4. `http://127.0.0.1:8000/api/lead`, tipo POST, para crear un nuevo candidato, se envia token en la petición
5. `http://127.0.0.1:8000/api/lead/5`, tipo GET,  para traer la información de un candidato especifico, se envia token en la petición

**Nota:**
1. En la carpeta Endpoints estan los `endpoints`, para probar

